package cz.cvut.fel.omo.party.implementations;

import cz.cvut.fel.omo.blockchain.Blockchain;
import cz.cvut.fel.omo.channel.Request;
import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.logging.Logger;
import cz.cvut.fel.omo.party.Party;
import cz.cvut.fel.omo.util.Certificate;
import cz.cvut.fel.omo.operation.*;
import cz.cvut.fel.omo.food.implementations.*;

public class Farmer extends Party {

    public Farmer(String name, Blockchain blockchain) {
        super(name, blockchain);
    }

    /**
     * @param request
     */
    @Override
    protected void processRequest(Request request) {
        Food food;
        int days;
        String fertiliser;
        String place;

        switch (request.getExpectedFood()) {
            case WHEAT:
                food = growWheat(request.getWeight());
                days = 10;
                fertiliser = "N20";
                place = "Brno";
                break;
            case SALAT:
                food = growSalat(request.getWeight());
                days = 20;
                fertiliser = "P3HC";
                place = "Ostrava";
                break;
            case POTATOE:
                food = growPotatoe(request.getWeight());
                days = 30;
                fertiliser = "Si2O";
                place = "Plzen";
                break;
            default:
                return;
        }
        Logger.logParty(this, food.getState().getState().getName() + " is ready for transaction");
        createTransaction(new GrowOperation(food, this, days, fertiliser, place), request.getPrice(), this,
                request.getSender());
    }

    /**
     * @return Certificate
     */
    protected Certificate generateCertificate() {
        return new Certificate("CZ");
    }

    /**
     * @param weight
     * @return Food
     */
    protected Food growWheat(int weight) {
        Food food = new Food(generateCertificate(), weight);
        food.setState(new Wheat(food));
        return food;
    }

    /**
     * @param weight
     * @return Food
     */
    protected Food growSalat(int weight) {
        Food food = new Food(generateCertificate(), weight);
        food.setState(new Salat(food));
        return food;
    }

    /**
     * @param weight
     * @return Food
     */
    protected Food growPotatoe(int weight) {
        Food food = new Food(generateCertificate(), weight);
        food.setState(new Potatoe(food));
        return food;
    }
}
