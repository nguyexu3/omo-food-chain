package cz.cvut.fel.omo.party.implementations;

import java.util.Random;
import cz.cvut.fel.omo.blockchain.Blockchain;
import cz.cvut.fel.omo.channel.Request;
import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.logging.Logger;
import cz.cvut.fel.omo.party.Party;
import cz.cvut.fel.omo.operation.*;

public class Storage extends Party {

    public Storage(String name, Blockchain blockchain) {
        super(name, blockchain);
    }

    /**
     * Update the state to packaged
     * 
     * @param request
     */
    @Override
    protected void processRequest(Request request) {
        Random random = new Random();

        Food food = request.getFood();
        food.getState().updateState();

        Logger.logParty(this, food.getState().getState().getName() + " is ready for transaction");
        createTransaction(new StoreOperation(food, this, random.nextInt(10), "10C", "80%", "Praha",
                request.getFood().getWeight() + 10), request.getPrice(), request.getSender(), this);
    }
}
