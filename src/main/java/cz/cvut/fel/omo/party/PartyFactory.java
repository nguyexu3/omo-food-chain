package cz.cvut.fel.omo.party;

import cz.cvut.fel.omo.blockchain.Blockchain;
import cz.cvut.fel.omo.party.implementations.*;

public class PartyFactory {

    private Blockchain blockChain;

    public PartyFactory(Blockchain blockChain) {
        this.blockChain = blockChain;
    }

    /**
     * @param name
     * @return Farmer
     */
    public Farmer createFarmer(String name) {
        return new Farmer(name, blockChain);
    }

    /**
     * @param name
     * @return Consumer
     */
    public Consumer createConsumer(String name) {
        return new Consumer(name, blockChain);
    }

    /**
     * @param name
     * @return Processor
     */
    public Processor createProcessor(String name) {
        return new Processor(name, blockChain);
    }

    /**
     * @param name
     * @return Processor
     */
    public Storage createStorage(String name) {
        return new Storage(name, blockChain);
    }
}
