package cz.cvut.fel.omo.party.implementations;

import cz.cvut.fel.omo.blockchain.Blockchain;
import cz.cvut.fel.omo.channel.Request;
import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.food.implementations.Potatoe;
import cz.cvut.fel.omo.logging.Logger;
import cz.cvut.fel.omo.party.Party;
import cz.cvut.fel.omo.operation.*;

public class Processor extends Party {

    public Processor(String name, Blockchain blockchain) {
        super(name, blockchain);
    }

    /**
     * First we get the food to have the ownership, process it and then trade it
     * back to the owner
     * 
     * @param request
     */
    @Override
    protected void processRequest(Request request) {
        switch (request.getExpectedFood()) {
            case FRIES:
                tradeSubject(request);
                Food food = new Food(request.getFood().getCertificate(), request.getFood().getWeight());
                food.setState(new Potatoe(food));
                request.setFood(food);
                cleanPotatoe(request.getFood(), request);
                fryPotatoe(request.getFood(), request);
                tradeSubjectBack(request);
                break;
            default:
                break;
        }
    }

    /**
     * @param request
     */
    protected void tradeSubject(Request request) {
        createTransaction(new TradeOperation(request.getFood(), this), 0, request.getSender(), this);
    }

    /**
     * @param request
     */
    protected void tradeSubjectBack(Request request) {
        createTransaction(new TradeOperation(request.getFood(), this), request.getPrice(), this, request.getSender());
    }

    /**
     * @param food
     * @param request
     */
    protected void cleanPotatoe(Food food, Request request) {
        food.getState().updateState();
        Logger.logParty(this, food.getState().getState().getName() + " is ready for transaction");
        createTransaction(new CleanOperation(food, this, EMethodType.MACHINE), 0, this, this);
    }

    /**
     * @param food
     * @param request
     */
    protected void fryPotatoe(Food food, Request request) {
        food.getState().updateState();
        food.getState().updateState();
        Logger.logParty(this, food.getState().getState().getName() + " is ready for transaction");
        createTransaction(new FryOperation(food, this, 2), 0, this, this);
    }
}
