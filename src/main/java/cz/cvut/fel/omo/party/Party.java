package cz.cvut.fel.omo.party;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;
import cz.cvut.fel.omo.blockchain.Blockchain;
import cz.cvut.fel.omo.blockchain.Transaction;
import cz.cvut.fel.omo.channel.Request;
import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.logging.Logger;
import cz.cvut.fel.omo.operation.Operation;
import cz.cvut.fel.omo.util.Signature;

public abstract class Party {

    private Signature signature;
    private String name;
    private Blockchain blockchain;
    private Request acceptedRequest;
    private Queue<Request> requests = new LinkedBlockingDeque<>();

    public Party(String name, Blockchain blockchain) {
        this.name = name;
        this.blockchain = blockchain;
        this.signature = new Signature();
    }

    /**
     * @param request
     */
    public void recieveRequest(Request request) {
        requests.add(request);
    }

    /**
     * @param request
     */
    public void cancelRequest(Request request) {
        requests.remove(request);
    }

    /**
     * accepts the first request from the queue if theres no waiting request for
     * processing. Then tells the channel to emit and remove the request from other
     * subscribers
     */
    public void accept() {
        Request request = requests.poll();
        if (request != null && acceptedRequest == null) {
            Logger.logParty(this, "Accepting the request from " + request.getSender().getName());
            request.getChannel().accept(this, request);
            this.acceptedRequest = request;
        }
    }

    public void process() {
        if (acceptedRequest != null) {
            Logger.logParty(this, "Processing the request from " + acceptedRequest.getSender().getName());
            this.processRequest(acceptedRequest);
        }
    }

    /**
     * Eg. usage: trading, processing
     * 
     * @param food
     */
    public void process(Food food) {
        if (acceptedRequest != null) {
            Logger.logParty(this,
                    "Processing the request with a specific food from " + acceptedRequest.getSender().getName());
            acceptedRequest.setFood(food);
            this.processRequest(acceptedRequest);
        }
    }

    /**
     * We instantly mine the block after addint an transaction to simulate real time
     * blockchain
     * 
     * @param operation
     * @param price
     * @param from
     * @param to
     */
    protected void createTransaction(Operation operation, int price, Party from, Party to) {
        blockchain.addPendingTransaction(new Transaction(operation, price, from, to,
                signature.sign(from.getSignature().getPublicKey() + to.getSignature().getPublicKey() + price)));
        blockchain.minePendingTransactions();
    }

    /**
     * method where we create the operation
     * 
     * @param request
     */
    protected abstract void processRequest(Request request);

    /**
     * @return Signature
     */
    public Signature getSignature() {
        return signature;
    }

    /**
     * @return String
     */
    public String getName() {
        return name;
    }
}
