package cz.cvut.fel.omo.party.implementations;

import cz.cvut.fel.omo.blockchain.Blockchain;
import cz.cvut.fel.omo.channel.Request;
import cz.cvut.fel.omo.logging.Logger;
import cz.cvut.fel.omo.party.Party;
import cz.cvut.fel.omo.operation.*;

public class Consumer extends Party {

    public Consumer(String name, Blockchain blockchain) {
        super(name, blockchain);
    }

    /**
     * @param request
     */
    @Override
    protected void processRequest(Request request) {
        Logger.logParty(this, request.getFood().getState().getState().getName() + " is ready for transaction");
        createTransaction(new TradeOperation(request.getFood(), this), request.getPrice(), this, request.getSender());
    }
}
