package cz.cvut.fel.omo.util;

import java.util.UUID;

public class Signature {

    private String publicKey;
    private String privateKey;

    public Signature() {
        this.publicKey = UUID.randomUUID().toString();
        this.privateKey = UUID.randomUUID().toString();
    }

    /**
     * Face encryption
     * 
     * @return String
     */
    public String sign(String message) {
        return message + privateKey;
    }

    /**
     * Fake verification
     * 
     * @param data
     * @param publicKey
     * @return boolean
     */
    public boolean verify(String message, String publicKey) {
        return true;
    }

    /**
     * @return String
     */
    public String getPublicKey() {
        return publicKey;
    }
}
