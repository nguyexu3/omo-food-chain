package cz.cvut.fel.omo.util;

import java.util.UUID;

/**
 * The main identification of a particuallar food
 */
public class Certificate {

    private String id;
    private String country;

    public Certificate(String country) {
        this.id = UUID.randomUUID().toString();
        this.country = country;
    }

    /**
     * Used for comparing two certificates
     * 
     * @param a certificate
     * @param b certificate
     * @return boolean
     */
    public boolean isEqual(Certificate a, Certificate b) {
        return (a.getId() == b.getId()) && (a.getCountry() == b.getCountry());
    }

    /**
     * @return String
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return String
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country
     */
    public void setCountry(String country) {
        this.country = country;
    }
}
