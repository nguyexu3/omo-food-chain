package cz.cvut.fel.omo;

import cz.cvut.fel.omo.blockchain.Block;
import cz.cvut.fel.omo.blockchain.Blockchain;
import cz.cvut.fel.omo.blockchain.BlockchainProvider;
import cz.cvut.fel.omo.blockchain.Transaction;
import cz.cvut.fel.omo.channel.ChannelFactory;
import cz.cvut.fel.omo.party.PartyFactory;
import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.food.implementations.*;
import cz.cvut.fel.omo.logging.Report;
import cz.cvut.fel.omo.operation.Operation;
import cz.cvut.fel.omo.operation.TradeOperation;
import cz.cvut.fel.omo.party.implementations.*;
import cz.cvut.fel.omo.util.Certificate;
import cz.cvut.fel.omo.channel.implementations.*;

public class Main {
    public static void main(String[] args) {
        simpleProductionSimulation();
        simpleTradeSimulation();
        complexFriesSimulation();
        invalidTransactionsSimulation();
        blockchainTemperSimulation();

        Report report = new Report(BlockchainProvider.getInstance());
        report.generateReports();
    }

    public static void simpleProductionSimulation() {
        System.out.println("");
        System.out.println("--- Starting: Simple production simulation ---");
        System.out.println("");

        ChannelFactory channelFactory = new ChannelFactory();
        Blockchain blockchain = BlockchainProvider.getInstance();
        PartyFactory partyFactory = new PartyFactory(blockchain);

        VegetableChannel channel1 = channelFactory.createVegetableChannel("Zeleninove trziste - Andel");

        Farmer farmerA = partyFactory.createFarmer("FarmerA");
        Farmer farmerB = partyFactory.createFarmer("FarmerB");
        Consumer consumerA = partyFactory.createConsumer("ConsumerA");

        channel1.subscribe(farmerA);
        channel1.subscribe(farmerB);

        channel1.requestSalad(consumerA, 100, 10);
        farmerB.accept();
        farmerB.process();

        System.out.println("");
        System.out.println("--- Simulation finished ---");
        System.out.println("");
    }

    public static void simpleTradeSimulation() {
        System.out.println("");
        System.out.println("--- Starting: Simple trade simulation ---");
        System.out.println("");

        ChannelFactory channelFactory = new ChannelFactory();
        Blockchain blockchain = BlockchainProvider.getInstance();
        PartyFactory partyFactory = new PartyFactory(blockchain);

        VegetableChannel channel2 = channelFactory.createVegetableChannel("Farmarske trziste");
        VegetableChannel channel3 = channelFactory.createVegetableChannel("Makro");

        Farmer farmerC = partyFactory.createFarmer("FarmerC");
        Consumer consumerB = partyFactory.createConsumer("ConsumerB");
        Consumer consumerC = partyFactory.createConsumer("ConsumerC");

        channel2.subscribe(farmerC);
        channel2.subscribe(consumerB);
        channel3.subscribe(consumerB);
        channel3.subscribe(consumerC);

        channel2.requestPotatoe(consumerB, 300, 20);
        farmerC.accept();
        farmerC.process();

        channel3.requestPotatoe(consumerC, 200, 50);
        consumerB.accept();
        consumerB.process(blockchain.getOwnedFood(consumerB, new Potatoe(new Food(null, 200))));

        System.out.println("");
        System.out.println("--- Simulation finished ---");
        System.out.println("");
    }

    public static void complexFriesSimulation() {
        System.out.println("");
        System.out.println("--- Starting: Complex fries simulation ---");
        System.out.println("");

        ChannelFactory channelFactory = new ChannelFactory();
        Blockchain blockchain = BlockchainProvider.getInstance();
        PartyFactory partyFactory = new PartyFactory(blockchain);

        VegetableChannel channel4 = channelFactory.createVegetableChannel("Albert");
        FriesChannel channel5 = channelFactory.createFriesChannel("Mcdonald market");
        StorageChannel channel6 = channelFactory.createStorageChannel("Skladiste letnany");

        Farmer farmerD = partyFactory.createFarmer("FarmerD");
        Processor processorA = partyFactory.createProcessor("Mcdonald");
        Consumer consumerD = partyFactory.createConsumer("ConsumerD");
        Storage storageA = partyFactory.createStorage("Zasilovka");

        channel4.subscribe(farmerD);
        channel4.subscribe(consumerD);
        channel5.subscribe(consumerD);
        channel5.subscribe(processorA);
        channel6.subscribe(consumerD);
        channel6.subscribe(storageA);

        channel4.requestPotatoe(consumerD, 200, 10);
        farmerD.accept();
        farmerD.process();

        channel5.requestFries(consumerD, blockchain.getOwnedFood(consumerD, new Potatoe(new Food(null, 200))), 20);
        processorA.accept();
        processorA.process();

        channel6.requestStorage(consumerD, blockchain.getOwnedFood(consumerD, new Fries(new Food(null, 200))), 10);
        storageA.accept();
        storageA.process();

        System.out.println("");
        System.out.println("--- Simulation finished ---");
        System.out.println("");
    }

    public static void invalidTransactionsSimulation() {
        System.out.println("");
        System.out.println("--- Starting: Invalid transactions simulation ---");
        System.out.println("");

        ChannelFactory channelFactory = new ChannelFactory();
        Blockchain blockchain = BlockchainProvider.getInstance();
        PartyFactory partyFactory = new PartyFactory(blockchain);

        VegetableChannel channel7 = channelFactory.createVegetableChannel("Farmarske trziste");
        VegetableChannel channel8 = channelFactory.createVegetableChannel("Makro");

        Farmer farmerE = partyFactory.createFarmer("FarmerE");
        Consumer consumerE = partyFactory.createConsumer("ConsumerE");
        Consumer consumerF = partyFactory.createConsumer("ConsumerF");
        Consumer consumerG = partyFactory.createConsumer("ConsumerG");

        channel7.subscribe(farmerE);
        channel7.subscribe(consumerE);
        channel8.subscribe(consumerE);
        channel8.subscribe(consumerF);

        channel7.requestPotatoe(consumerE, 300, 20);
        farmerE.accept();
        farmerE.process();

        channel8.requestPotatoe(consumerF, 200, 50);
        consumerE.accept();
        // We store this food in a variable to resell again
        Food food = blockchain.getOwnedFood(consumerE, new Potatoe(new Food(null, 200)));
        consumerE.process(food);

        channel8.requestPotatoe(consumerG, 200, 80);
        consumerE.accept();
        consumerE.process(food);

        channel8.requestPotatoe(consumerF, 200, 50);
        consumerE.accept();

        // Selling fake food from poland as it was from our blockchain system
        Food fakeFood = new Food(new Certificate("PL"), 200);
        fakeFood.setState(new Potatoe(fakeFood));
        consumerE.process(fakeFood);

        System.out.println("");
        System.out.println("--- Simulation finished ---");
        System.out.println("");
    }

    public static void blockchainTemperSimulation() {
        System.out.println("");
        System.out.println("--- Starting: Blockchain temper simulation ---");
        System.out.println("");

        // we are not using the provider to influence the previrous blockchain
        Blockchain blockchain = new Blockchain();

        ChannelFactory channelFactory = new ChannelFactory();
        PartyFactory partyFactory = new PartyFactory(blockchain);

        VegetableChannel channel1 = channelFactory.createVegetableChannel("Zeleninove trziste - Andel");

        Farmer farmerA = partyFactory.createFarmer("FarmerA");
        Farmer farmerB = partyFactory.createFarmer("FarmerB");
        Consumer consumerA = partyFactory.createConsumer("ConsumerA");

        channel1.subscribe(farmerB);
        channel1.subscribe(farmerA);

        channel1.requestSalad(consumerA, 100, 10);
        farmerB.accept();
        farmerB.process();

        // now for example farmerB wants to increase his price so he tempters with the
        // blockchain
        Block block = blockchain.getChain().get(1);
        Food fakeFood = new Food(new Certificate("PL"), 10);
        fakeFood.setState(new Potatoe(fakeFood));
        Operation fakeOperation = new TradeOperation(fakeFood, farmerB);
        block.getTransactions().add(new Transaction(fakeOperation, 2000, farmerB, farmerA, "fake signature"));

        channel1.requestSalad(consumerA, 130, 20);
        farmerA.accept();
        farmerA.process();

        System.out.println("");
        System.out.println("--- Simulation finished ---");
        System.out.println("");
    }
}
