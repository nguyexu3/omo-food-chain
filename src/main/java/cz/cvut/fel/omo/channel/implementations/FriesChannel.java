package cz.cvut.fel.omo.channel.implementations;

import cz.cvut.fel.omo.channel.Channel;
import cz.cvut.fel.omo.channel.Request;
import cz.cvut.fel.omo.food.EFoodState;
import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.logging.Logger;
import cz.cvut.fel.omo.party.Party;

public class FriesChannel extends Channel {

    public FriesChannel(String name) {
        super(name);
    }

    /**
     * @param sender
     * @param food
     * @param price
     */
    public void requestFries(Party sender, Food food, int price) {
        Logger.logChannel(this, sender.getName() + " is requesting fry process");
        propagate(new Request(sender, EFoodState.FRIES, food, this, food.getWeight(), price));
    }
}
