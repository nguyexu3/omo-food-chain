package cz.cvut.fel.omo.channel.implementations;

import cz.cvut.fel.omo.channel.Channel;
import cz.cvut.fel.omo.channel.Request;
import cz.cvut.fel.omo.food.EFoodState;
import cz.cvut.fel.omo.logging.Logger;
import cz.cvut.fel.omo.party.Party;

public class VegetableChannel extends Channel {

    public VegetableChannel(String name) {
        super(name);
    }

    /**
     * @param sender
     * @param weight
     * @param price
     */
    public void requestSalad(Party sender, int weight, int price) {
        this.sendBasicRequest(EFoodState.SALAT, sender, weight, price);
    }

    /**
     * @param sender
     * @param weight
     * @param price
     */
    public void requestPotatoe(Party sender, int weight, int price) {
        this.sendBasicRequest(EFoodState.POTATOE, sender, weight, price);
    }

    /**
     * @param sender
     * @param weight
     * @param price
     */
    public void requestWheat(Party sender, int weight, int price) {
        this.sendBasicRequest(EFoodState.WHEAT, sender, weight, price);
    }

    /**
     * @param eFoodState
     * @param sender
     * @param weight
     * @param price
     */
    protected void sendBasicRequest(EFoodState eFoodState, Party sender, int weight, int price) {
        Logger.logChannel(this, sender.getName() + " is requesting " + eFoodState.getName());
        propagate(new Request(sender, eFoodState, this, weight, price));
    }
}
