package cz.cvut.fel.omo.channel.implementations;

import cz.cvut.fel.omo.channel.Channel;
import cz.cvut.fel.omo.food.EFoodState;
import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.logging.Logger;
import cz.cvut.fel.omo.operation.EOperation;
import cz.cvut.fel.omo.party.Party;

public class DeliveryChannel extends Channel {

    public DeliveryChannel(String name) {
        super(name);
    }
}
