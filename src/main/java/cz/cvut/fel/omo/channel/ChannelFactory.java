package cz.cvut.fel.omo.channel;

import cz.cvut.fel.omo.channel.implementations.*;

public class ChannelFactory {

    /**
     * @param name
     * @return VegetableChannel
     */
    public VegetableChannel createVegetableChannel(String name) {
        return new VegetableChannel(name);
    }

    /**
     * @param name
     * @return MeatChannel
     */
    public MeatChannel createMeatChannel(String name) {
        return new MeatChannel(name);
    }

    /**
     * @param name
     * @return StorageChannel
     */
    public StorageChannel createStorageChannel(String name) {
        return new StorageChannel(name);
    }

    /**
     * @param name
     * @return SandwichChannel
     */
    public SandwichChannel createSandwichChannel(String name) {
        return new SandwichChannel(name);
    }

    /**
     * @param name
     * @return FriesChannel
     */
    public FriesChannel createFriesChannel(String name) {
        return new FriesChannel(name);
    }

    /**
     * @param name
     * @return DeliveryChannel
     */
    public DeliveryChannel createDeliveryChannel(String name) {
        return new DeliveryChannel(name);
    }

    /**
     * @param name
     * @return BakeryChannel
     */
    public BakeryChannel createBakeryChannel(String name) {
        return new BakeryChannel(name);
    }
}
