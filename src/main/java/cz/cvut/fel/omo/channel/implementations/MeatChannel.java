package cz.cvut.fel.omo.channel.implementations;

import cz.cvut.fel.omo.channel.Channel;
import cz.cvut.fel.omo.channel.Request;
import cz.cvut.fel.omo.food.EFoodState;
import cz.cvut.fel.omo.logging.Logger;
import cz.cvut.fel.omo.party.Party;

public class MeatChannel extends Channel {

    public MeatChannel(String name) {
        super(name);
    }

    /**
     * @param sender
     * @param weight
     * @param price
     */
    public void requestChicken(Party sender, int weight, int price) {
        this.sendBasicRequest(EFoodState.CHICKEN, sender, weight, price);
    }

    /**
     * @param eFoodState
     * @param sender
     * @param weight
     * @param price
     */
    protected void sendBasicRequest(EFoodState eFoodState, Party sender, int weight, int price) {
        Logger.logChannel(this, sender.getName() + " is requesting " + eFoodState.getName());
        propagate(new Request(sender, eFoodState, this, weight, price));
    }
}
