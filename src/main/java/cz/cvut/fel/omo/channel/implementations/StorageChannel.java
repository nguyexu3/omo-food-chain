package cz.cvut.fel.omo.channel.implementations;

import cz.cvut.fel.omo.channel.Channel;
import cz.cvut.fel.omo.channel.Request;
import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.food.EFoodState;
import cz.cvut.fel.omo.logging.Logger;
import cz.cvut.fel.omo.party.Party;

public class StorageChannel extends Channel {

    public StorageChannel(String name) {
        super(name);
    }

    /**
     * @param sender
     * @param food
     * @param price
     */
    public void requestStorage(Party sender, Food food, int price) {
        Logger.logChannel(this, sender.getName() + " is requesting storage");
        propagate(new Request(sender, EFoodState.PACKAGED_FRIES, food, this, food.getWeight(), price));
    }
}
