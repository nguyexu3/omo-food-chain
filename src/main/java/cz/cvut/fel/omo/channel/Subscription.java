package cz.cvut.fel.omo.channel;

import java.util.HashSet;
import java.util.Set;
import cz.cvut.fel.omo.operation.EOperation;
import cz.cvut.fel.omo.party.Party;

/**
 * A wrapper class which holds the subcriber and additional properties which is
 * used to filter out during request emiting
 */
public class Subscription {

    private Party subscriber;
    private Set<EOperation> unSubscribedOperations;

    public Subscription(Party subscriber) {
        unSubscribedOperations = new HashSet<EOperation>();
        this.subscriber = subscriber;
    }

    /**
     * @return Party
     */
    public Party getSubscriber() {
        return subscriber;
    }

    /**
     * @param subscriber
     */
    public void setSubscriber(Party subscriber) {
        this.subscriber = subscriber;
    }

    /**
     * @return Set<EOperation>
     */
    public Set<EOperation> getUnSubscribedOperations() {
        return unSubscribedOperations;
    }
}
