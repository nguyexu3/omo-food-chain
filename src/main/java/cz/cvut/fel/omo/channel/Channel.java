package cz.cvut.fel.omo.channel;

import java.util.ArrayList;
import java.util.List;
import cz.cvut.fel.omo.logging.Logger;
import cz.cvut.fel.omo.party.Party;

public class Channel {

    private String name;
    private List<Subscription> subscriptions;
    private List<Request> requests;

    public Channel(String name) {
        this.name = name;
        this.subscriptions = new ArrayList<>();
        this.requests = new ArrayList<>();
    }

    /**
     * @param party
     */
    public void subscribe(Party party) {
        if (!isSubscribed(party)) {
            Logger.logChannel(this, party.getName() + " subscribed to this channel");
            subscriptions.add(new Subscription(party));
        }
    }

    /**
     * @param party
     */
    public void unSubscribe(Party party) {
        for (Subscription s : subscriptions) {
            if (s.getSubscriber() == party) {
                Logger.logChannel(this, party.getName() + " unsubscribed from this channel");
                subscriptions.remove(s);
                return;
            }
        }
    }

    /**
     * When someone accepts a request, remove it from the subscribers queue
     * 
     * @param party
     * @param request
     */
    public void accept(Party party, Request request) {
        if (requests.contains(request)) {
            for (Subscription s : subscriptions) {
                if (!s.getUnSubscribedOperations().contains(request.getOperation())) {
                    s.getSubscriber().cancelRequest(request);
                }
            }
            requests.remove(request);
        }
    }

    /**
     * @param request
     */
    public void propagate(Request request) {
        Logger.logChannel(this, "propagating request");
        requests.add(request);
        for (Subscription s : subscriptions) {
            if (!s.getUnSubscribedOperations().contains(request.getOperation())) {
                s.getSubscriber().recieveRequest(request);
            }
        }
    }

    /**
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param party
     * @return boolean
     */
    private boolean isSubscribed(Party party) {
        for (Subscription s : subscriptions) {
            if (s.getSubscriber() == party) {
                return true;
            }
        }
        return false;
    }
}
