package cz.cvut.fel.omo.channel;

import cz.cvut.fel.omo.food.EFoodState;
import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.operation.EOperation;
import cz.cvut.fel.omo.party.Party;

public class Request {

    private Party sender;
    private EOperation operation;
    private EFoodState expectedFood;
    private Food food;
    private Channel channel;
    private int weight;
    private int price;

    public Request(Party sender, EFoodState expectedFood, Channel channel, int weight, int price) {
        this.sender = sender;
        this.expectedFood = expectedFood;
        this.channel = channel;
        this.weight = weight;
        this.price = price;
    }

    public Request(Party sender, EFoodState expectedFood, Food food, Channel channel, int weight, int price) {
        this.sender = sender;
        this.expectedFood = expectedFood;
        this.food = food;
        this.channel = channel;
        this.weight = weight;
        this.price = price;
    }

    /**
     * @return Party
     */
    public Party getSender() {
        return sender;
    }

    /**
     * @return EOperation
     */
    public EOperation getOperation() {
        return operation;
    }

    /**
     * @return EFoodState
     */
    public EFoodState getExpectedFood() {
        return expectedFood;
    }

    /**
     * @return Food
     */
    public Food getFood() {
        return food;
    }

    /**
     * @param food
     */
    public void setFood(Food food) {
        this.food = food;
    }

    /**
     * @return Channel
     */
    public Channel getChannel() {
        return channel;
    }

    /**
     * @return int
     */
    public int getWeight() {
        return weight;
    }

    /**
     * @return int
     */
    public int getPrice() {
        return price;
    }
}
