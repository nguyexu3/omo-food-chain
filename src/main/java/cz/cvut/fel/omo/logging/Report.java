package cz.cvut.fel.omo.logging;

import cz.cvut.fel.omo.blockchain.Block;
import cz.cvut.fel.omo.blockchain.Blockchain;
import cz.cvut.fel.omo.blockchain.Transaction;
import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.operation.EOperation;
import cz.cvut.fel.omo.operation.Operation;
import cz.cvut.fel.omo.party.Party;
import cz.cvut.fel.omo.util.Certificate;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Report {

    private Blockchain blockchain;

    public Report(Blockchain blockchain) {
        this.blockchain = blockchain;
    }

    public void generateReports() {
        System.out.println("Generating reports... ");
        generatePartiesReport();
        generateFoodChainReport();
        generateSecurityReport();
        generateTransactionReport();
        System.out.println("Finished, reports generated at target/classes/");
        System.out.println("");
    }

    /**
     * @param name
     * @return PrintWriter
     */
    private PrintWriter getFile(String name) {
        String path = this.getClass().getResource(File.separator).getPath() + name + "_report.txt";
        File file = new File(path);
        try {
            file.createNewFile();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        try {
            return new PrintWriter(file);
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return null;
    }

    private void generatePartiesReport() {
        PrintWriter pw = getFile("parties");

        Map<Party, List<Transaction>> parties = new HashMap<>();
        for (Block b : blockchain.getChain()) {
            for (Transaction t : b.getTransactions()) {
                Party p = t.getOperation().getParty();
                if (!parties.containsKey(p)) {
                    parties.put(p, new ArrayList<Transaction>());
                }
                parties.get(p).add(t);
            }
        }

        for (Map.Entry<Party, List<Transaction>> entry : parties.entrySet()) {
            pw.println(entry.getKey().getName());
            for (Transaction t : entry.getValue()) {
                Operation o = t.getOperation();
                pw.println("[operation]: " + o.getType().getName() + ", [food]: "
                        + o.getFood().getState().getState().getName() + ", [amount]: " + o.getFood().getWeight() + "kg"
                        + ", [price]: " + t.getPrice() + "$");
            }
            pw.println("");
        }

        pw.close();
    }

    private void generateFoodChainReport() {
        PrintWriter pw = getFile("food_chain");

        Map<Certificate, List<Transaction>> foods = new HashMap<>();
        for (Block b : blockchain.getChain()) {
            for (Transaction t : b.getTransactions()) {
                Certificate c = t.getOperation().getFood().getCertificate();
                if (!foods.containsKey(c)) {
                    foods.put(c, new ArrayList<Transaction>());
                }
                foods.get(c).add(t);
            }
        }

        for (Map.Entry<Certificate, List<Transaction>> entry : foods.entrySet()) {
            Certificate c = entry.getKey();
            pw.println("Food certificate - " + c.getId() + c.getCountry());
            for (Transaction t : entry.getValue()) {
                pw.println("[party]: " + t.getFrom().getName());
                Operation o = t.getOperation();
                pw.println("[food]: " + o.getFood().getState().getState().getName() + ", [operation]: "
                        + o.getType().getName() + ", [parameters]: " + o.getParameters());
            }
            pw.println("");
        }

        pw.close();
    }

    private void generateSecurityReport() {
        PrintWriter pw = getFile("security");

        for (Map.Entry<Party, Integer> entry : Logger.securityReport.entrySet()) {
            pw.println(entry.getKey().getName() + " has " + entry.getValue() + " failed transaction");
            pw.println("");
        }

        pw.close();
    }

    private void generateTransactionReport() {
        PrintWriter pw = getFile("transaction");

        List<Transaction> transactions = new ArrayList<>();

        for (Block b : blockchain.getChain()) {
            for (Transaction t : b.getTransactions()) {
                transactions.add(t);

                Operation o = t.getOperation();
                Food f = o.getFood();
                pw.println("[from]: " + t.getFrom().getName() + ", [money]: " + getBalance(transactions, t.getFrom())
                        + "$ , [amount]: " + getFoodAmount(transactions, t.getFrom(), f.getCertificate(), o.getType())
                        + " kg of " + f.getState().getState().getName());
                pw.println("[to]: " + t.getTo().getName() + ", [money]: " + getBalance(transactions, t.getTo())
                        + "$ , [amount]: " + getFoodAmount(transactions, t.getTo(), f.getCertificate(), o.getType())
                        + " kg of " + f.getState().getState().getName());
                pw.println("");
            }
        }

        pw.close();
    }

    /**
     * @param transactions
     * @param party
     * @return int
     */
    private int getBalance(List<Transaction> transactions, Party party) {
        int balance = 0;

        for (Transaction t : transactions) {
            int price = t.getPrice();

            if (t.getFrom() == party || t.getTo() == party) {
                if (t.getOperation().getParty() == party) {
                    balance += price;
                } else {
                    balance -= price;
                }
            }

        }

        return balance;
    }

    /**
     * @param transactions
     * @param party
     * @param certificate
     * @param eOperation
     * @return int
     */
    private int getFoodAmount(List<Transaction> transactions, Party party, Certificate certificate,
            EOperation eOperation) {
        int totalAmount = 0;

        for (Transaction t : transactions) {
            // Skip for growing, because the party creates new food
            if (t.getFrom() == party && (eOperation == EOperation.GROW || eOperation == EOperation.BREED)) {
                continue;
            }

            if (t.getOperation().getFood().getCertificate() == certificate) {
                int amount = t.getOperation().getFood().getWeight();

                if (t.getFrom() == party) {
                    totalAmount -= amount;
                }

                if (t.getTo() == party) {
                    totalAmount += amount;
                }
            }
        }

        return totalAmount;
    }
}
