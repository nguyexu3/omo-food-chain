package cz.cvut.fel.omo.logging;

import cz.cvut.fel.omo.channel.Channel;
import cz.cvut.fel.omo.operation.Operation;
import cz.cvut.fel.omo.party.Party;
import java.util.HashMap;
import java.util.Map;

/**
 * Static utility logger class which additionally holds the info of failed
 * transactions
 */
public class Logger {

    public static Map<Party, Integer> securityReport = new HashMap<>();

    /**
     * @param channel
     * @param message
     */
    public static void logChannel(Channel channel, String message) {
        System.out.println("[CHANNEL] " + "(channel: " + channel.getName() + ") " + message);
    }

    /**
     * @param party
     * @param message
     */
    public static void logParty(Party party, String message) {
        System.out.println("[PARTY] " + "(party: " + party.getName() + ") " + message);
    }

    /**
     * @param operation
     * @param message
     */
    public static void logOperation(Operation operation, String message) {
        System.out.println("[OPERATION] " + "(operation: " + operation.getType().getName() + ") " + message);
    }

    /**
     * @param message
     */
    public static void logTransaction(String message) {
        System.out.println("[TRANSACTION] " + message);
    }

    /**
     * @param message
     */
    public static void logBlockChain(String message) {
        System.out.println("[BLOCKCHAIN] " + message);
    }

    /**
     * @param message
     */
    public static void logError(String message) {
        System.out.println("[ERROR:] " + message);
    }

    /**
     * @param party
     * @param message
     */
    public static void logError(Party party, String message) {
        if (!securityReport.containsKey(party)) {
            securityReport.put(party, 0);
        }
        securityReport.put(party, securityReport.get(party) + 1);
        System.out.println("[ERROR:] " + message);
    }
}
