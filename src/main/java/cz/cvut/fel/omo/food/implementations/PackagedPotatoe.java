package cz.cvut.fel.omo.food.implementations;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.food.FoodState;
import cz.cvut.fel.omo.food.EFoodState;

public class PackagedPotatoe extends FoodState {

    public PackagedPotatoe(Food food) {
        super(food);
        state = EFoodState.PACKAGED_POTATOE;
    }

    protected void changeToNextState() {
        food.setState(new Fries(food));
    }
}