package cz.cvut.fel.omo.food.implementations;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.food.FoodState;
import cz.cvut.fel.omo.food.EFoodState;

public class PackagedSandwich extends FoodState {

    public PackagedSandwich(Food food) {
        super(food);
        state = EFoodState.PACKAGED_SANDWICH;
    }

    protected void changeToNextState() {
    }
}
