package cz.cvut.fel.omo.food.implementations;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.food.FoodState;
import cz.cvut.fel.omo.food.EFoodState;

public class CleanedSalat extends FoodState {

    public CleanedSalat(Food food) {
        super(food);
        state = EFoodState.CLEANED_SALAT;
    }

    protected void changeToNextState() {
        food.setState(new PackagedSalat(food));
    }
}
