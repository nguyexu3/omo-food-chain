package cz.cvut.fel.omo.food;

public enum EFoodState {

    WHEAT("wheat"), FLOUR("flour"), BREAD("bread"), PACKAGED_BREAD("packaged bread"), CHICKEN("chicken"),
    RAW_CHICKEN("raw chicken"), BUTCHERED_CHICKEN("butchered chicken"), COOCKED_CHICKEN("cooked chicken"),
    PACKAGED_CHICKEN("packaged chicken"), SALAT("salat"), CLEANED_SALAT("cleaned salat"),
    PACKAGED_SALAT("packaged salat"), SANDWICH("sandwich"), PACKAGED_SANDWICH("packaged sandwich"), POTATOE("potatoe"),
    CLEANED_POTATOE("cleaned potatoe"), PACKAGED_POTATOE("packaged potatoe"), FRIES("fries"),
    PACKAGED_FRIES("packaged fries");

    String name;

    EFoodState(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
