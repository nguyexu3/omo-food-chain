package cz.cvut.fel.omo.food.implementations;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.food.FoodState;
import cz.cvut.fel.omo.food.EFoodState;

public class RawChicken extends FoodState {

    public RawChicken(Food food) {
        super(food);
        state = EFoodState.RAW_CHICKEN;
    }

    protected void changeToNextState() {
        food.setState(new ButcheredChicken(food));
    }
}
