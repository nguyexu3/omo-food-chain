package cz.cvut.fel.omo.food.implementations;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.food.FoodState;
import cz.cvut.fel.omo.food.EFoodState;

public class PackagedSalat extends FoodState {

    public PackagedSalat(Food food) {
        super(food);
        state = EFoodState.PACKAGED_SALAT;
    }

    protected void changeToNextState() {
    }
}
