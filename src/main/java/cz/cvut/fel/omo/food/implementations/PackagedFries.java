package cz.cvut.fel.omo.food.implementations;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.food.FoodState;
import cz.cvut.fel.omo.food.EFoodState;

public class PackagedFries extends FoodState {

    public PackagedFries(Food food) {
        super(food);
        state = EFoodState.PACKAGED_FRIES;
    }

    protected void changeToNextState() {
    }
}