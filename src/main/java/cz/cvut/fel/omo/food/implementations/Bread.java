package cz.cvut.fel.omo.food.implementations;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.food.FoodState;
import cz.cvut.fel.omo.food.EFoodState;

public class Bread extends FoodState {

    public Bread(Food food) {
        super(food);
        state = EFoodState.BREAD;
    }

    protected void changeToNextState() {
        food.setState(new PackagedBread(food));
    }
}
