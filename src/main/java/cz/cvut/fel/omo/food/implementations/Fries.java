package cz.cvut.fel.omo.food.implementations;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.food.FoodState;
import cz.cvut.fel.omo.food.EFoodState;

public class Fries extends FoodState {

    public Fries(Food food) {
        super(food);
        state = EFoodState.FRIES;
    }

    protected void changeToNextState() {
        food.setState(new PackagedFries(food));
    }
}