package cz.cvut.fel.omo.food.implementations;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.food.FoodState;
import cz.cvut.fel.omo.food.EFoodState;

public class PackagedChicken extends FoodState {

    public PackagedChicken(Food food) {
        super(food);
        state = EFoodState.PACKAGED_CHICKEN;
    }

    protected void changeToNextState() {
    }
}
