package cz.cvut.fel.omo.food.implementations;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.food.FoodState;
import cz.cvut.fel.omo.food.EFoodState;

public class Salat extends FoodState {

    public Salat(Food food) {
        super(food);
        state = EFoodState.SALAT;
    }

    protected void changeToNextState() {
        food.setState(new CleanedSalat(food));
    }
}
