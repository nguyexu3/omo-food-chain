package cz.cvut.fel.omo.food.implementations;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.food.FoodState;
import cz.cvut.fel.omo.food.EFoodState;

public class ButcheredChicken extends FoodState {

    public ButcheredChicken(Food food) {
        super(food);
        state = EFoodState.BUTCHERED_CHICKEN;
    }

    protected void changeToNextState() {
        food.setState(new PackagedChicken(food));
    }
}
