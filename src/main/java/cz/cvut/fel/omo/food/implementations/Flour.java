package cz.cvut.fel.omo.food.implementations;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.food.FoodState;
import cz.cvut.fel.omo.food.EFoodState;

public class Flour extends FoodState {

    public Flour(Food food) {
        super(food);
        state = EFoodState.FLOUR;
    }

    protected void changeToNextState() {
        food.setState(new Bread(food));
    }
}
