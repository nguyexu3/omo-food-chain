package cz.cvut.fel.omo.food.implementations;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.food.FoodState;
import cz.cvut.fel.omo.food.EFoodState;

public class Chicken extends FoodState {

    public Chicken(Food food) {
        super(food);
        state = EFoodState.CHICKEN;
    }

    protected void changeToNextState() {
        food.setState(new RawChicken(food));
    }
}
