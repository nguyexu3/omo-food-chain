package cz.cvut.fel.omo.food;

import cz.cvut.fel.omo.util.Certificate;

/**
 * A wrapper class for definying food, can be usefull if we want to sell a
 * specific amount of food instead of the whole food
 */
public class Food {

    private Certificate certificate;
    private int weight;
    private FoodState state;

    public Food(Certificate certificate, int weight) {
        this.certificate = certificate;
        this.weight = weight;
    }

    public void process() {
        state.updateState();
    }

    /**
     * @return Certificate
     */
    public Certificate getCertificate() {
        return certificate;
    }

    /**
     * @param certificate
     */
    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

    /**
     * @return int
     */
    public int getWeight() {
        return weight;
    }

    /**
     * @param weight
     */
    public void setWeight(int weight) {
        this.weight = weight;
    }

    /**
     * @return FoodState
     */
    public FoodState getState() {
        return state;
    }

    /**
     * @param state
     */
    public void setState(FoodState state) {
        this.state = state;
    }
}
