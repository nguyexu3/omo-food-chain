package cz.cvut.fel.omo.food.implementations;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.food.FoodState;
import cz.cvut.fel.omo.food.EFoodState;

public class CookedChicken extends FoodState {

    public CookedChicken(Food food) {
        super(food);
        state = EFoodState.COOCKED_CHICKEN;
    }

    protected void changeToNextState() {
        food.setState(new ButcheredChicken(food));
    }
}
