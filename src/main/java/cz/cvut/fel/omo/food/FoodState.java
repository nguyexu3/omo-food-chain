package cz.cvut.fel.omo.food;

/**
 * Holds the food state
 */
public abstract class FoodState {

    protected Food food;
    protected EFoodState state;

    public FoodState(Food food) {
        this.food = food;
    }

    public void updateState() {
        changeToNextState();
    }

    /**
     * @param getFood(
     */
    abstract protected void changeToNextState();

    /**
     * @return Food
     */
    public Food getFood() {
        return food;
    }

    /**
     * @param food
     */
    public void setFood(Food food) {
        this.food = food;
    }

    /**
     * @return EFoodState
     */
    public EFoodState getState() {
        return state;
    }

    /**
     * @param state
     */
    public void setState(EFoodState state) {
        this.state = state;
    }

}
