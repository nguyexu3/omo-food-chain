package cz.cvut.fel.omo.food.implementations;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.food.FoodState;
import cz.cvut.fel.omo.food.EFoodState;

public class Sandwich extends FoodState {

    public Sandwich(Food food) {
        super(food);
        state = EFoodState.SANDWICH;
    }

    protected void changeToNextState() {
        food.setState(new PackagedSandwich(food));
    }
}
