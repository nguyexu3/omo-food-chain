package cz.cvut.fel.omo.food.implementations;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.food.FoodState;
import cz.cvut.fel.omo.food.EFoodState;

public class PackagedBread extends FoodState {

    public PackagedBread(Food food) {
        super(food);
        state = EFoodState.PACKAGED_BREAD;
    }

    protected void changeToNextState() {
    }
}
