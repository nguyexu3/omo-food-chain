package cz.cvut.fel.omo.food.implementations;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.food.FoodState;
import cz.cvut.fel.omo.food.EFoodState;

public class Wheat extends FoodState {

    public Wheat(Food food) {
        super(food);
        state = EFoodState.WHEAT;
    }

    protected void changeToNextState() {
        food.setState(new Flour(food));
    }
}
