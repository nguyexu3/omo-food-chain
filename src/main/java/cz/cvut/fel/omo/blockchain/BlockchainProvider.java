package cz.cvut.fel.omo.blockchain;

/**
 * Singleton wrappper
 */
public class BlockchainProvider {

    private static Blockchain blockchain = null;

    /**
     * @return Blockchain
     */
    public static Blockchain getInstance() {
        if (blockchain == null) {
            blockchain = new Blockchain();
        }
        return blockchain;
    }
}
