package cz.cvut.fel.omo.blockchain;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.food.FoodState;
import cz.cvut.fel.omo.logging.Logger;
import cz.cvut.fel.omo.operation.EOperation;
import cz.cvut.fel.omo.operation.Operation;
import cz.cvut.fel.omo.party.Party;
import cz.cvut.fel.omo.util.Certificate;

public class Blockchain {

    private final static int difficulity = 2;

    private List<Block> chain;
    private List<Transaction> pendingTransactions;

    /**
     * create the first block
     */
    public Blockchain() {
        this.chain = new ArrayList<>();
        this.pendingTransactions = new ArrayList<>();
        chain.add(this.createGenesisBlock());
    }

    /**
     * @return Block
     */
    private Block createGenesisBlock() {
        Calendar genesisDate = GregorianCalendar.getInstance();
        genesisDate.set(2021, 1, 5);
        return new Block("", new ArrayList<Transaction>(), genesisDate.getTime());
    }

    /**
     * @param transaction
     */
    public void addPendingTransaction(Transaction transaction) {
        if (!transaction.isSignatureValid()) {
            Logger.logError(transaction.getFrom(), "Invalid signature");
            return;
        }
        if (!this.hasEnoughtFood(transaction)) {
            Logger.logError(transaction.getFrom(), "From party does not own enought food");
            return;
        }
        Logger.logBlockChain("adding transaction to blockchain");
        pendingTransactions.add(transaction);
    }

    /**
     * @return Block
     */
    private Block getLatestBlock() {
        return chain.get(chain.size() - 1);
    }

    /**
     * @return boolean
     */
    public boolean isChainValid() {
        for (int i = 1; i < chain.size(); i++) {
            Block curBlock = chain.get(i);
            Block prevBlock = chain.get(i - 1);

            if (!curBlock.getHash().equals(curBlock.calculateHash())) {
                return false;
            }

            if (!curBlock.getPreviousHash().equals(prevBlock.getHash())) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param party
     * @param foodState
     * @return Food
     */
    public Food getOwnedFood(Party party, FoodState foodState) {
        Map<Certificate, Integer> ownedFoods = new HashMap<>();

        for (Block b : chain) {
            for (Transaction t : b.getTransactions()) {
                Food f = t.getOperation().getFood();
                Party from = t.getFrom();
                Party to = t.getTo();

                if (f.getState().getState() == foodState.getState()) {
                    if (from == party || to == party && !ownedFoods.containsKey(f.getCertificate())) {
                        ownedFoods.put(f.getCertificate(), 0);
                    }
                    if (from == party) {
                        ownedFoods.put(f.getCertificate(), ownedFoods.get(f.getCertificate()) - f.getWeight());
                    }
                    if (to == party) {
                        ownedFoods.put(f.getCertificate(), ownedFoods.get(f.getCertificate()) + f.getWeight());
                    }
                }
            }
        }

        for (Map.Entry<Certificate, Integer> entry : ownedFoods.entrySet()) {
            if (entry.getValue() >= foodState.getFood().getWeight()) {
                Food food = new Food(entry.getKey(), foodState.getFood().getWeight());
                foodState.setFood(food);
                food.setState(foodState);
                return food;
            }
        }

        return null;
    }

    /**
     * @param transaction
     * @return boolean
     */
    private boolean hasEnoughtFood(Transaction transaction) {
        Operation operation = transaction.getOperation();
        Party party = transaction.getFrom();
        Food food = transaction.getOperation().getFood();
        int weight = transaction.getOperation().getFood().getWeight();

        if (operation.getType() == EOperation.GROW || operation.getType() == EOperation.BREED) {
            return true;
        }

        Food total = getFoodTotal(party, food.getCertificate());
        if (total.getWeight() >= weight) {
            return true;
        }

        return false;
    }

    /**
     * @param party
     * @param certificate
     * @return Food
     */
    private Food getFoodTotal(Party party, Certificate certificate) {
        Food total = new Food(certificate, 0);

        for (Block b : chain) {
            for (Transaction t : b.getTransactions()) {
                Food f = t.getOperation().getFood();
                Party from = t.getFrom();
                Party to = t.getTo();

                if (f.getCertificate() == certificate) {
                    if (from == party) {
                        total.setWeight(total.getWeight() - f.getWeight());
                    }
                    if (to == party) {
                        total.setWeight(total.getWeight() + f.getWeight());
                    }
                }
            }
        }

        return total;
    }

    public void minePendingTransactions() {
        if (!isChainValid()) {
            Logger.logError("Blockchain is not valid, chain needed to be reverted back");
            return;
        }
        if (pendingTransactions.size() == 0) {
            Logger.logBlockChain("No transactions to mine");
            return;
        }
        Block block = new Block(this.getLatestBlock().getHash(), pendingTransactions, new Date());
        block.mineBlock(difficulity);
        Logger.logBlockChain("Block mined");
        this.chain.add(block);
        this.pendingTransactions = new ArrayList<>();
    }

    /**
     * @return List<Block>
     */
    public List<Block> getChain() {
        return chain;
    }
}
