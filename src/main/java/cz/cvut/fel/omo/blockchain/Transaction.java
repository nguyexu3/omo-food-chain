package cz.cvut.fel.omo.blockchain;

import cz.cvut.fel.omo.operation.Operation;
import cz.cvut.fel.omo.party.Party;

public class Transaction {

    private Operation operation;
    private int price;
    private Party from;
    private Party to;
    private String signedSignature;

    public Transaction(Operation operation, int price, Party from, Party to, String signedSignature) {
        this.operation = operation;
        this.price = price;
        this.from = from;
        this.to = to;
        this.signedSignature = signedSignature;
    }

    /**
     * @return String
     */
    @Override
    public String toString() {
        return operation.getParameters() + signedSignature;
    }

    /**
     * uses fake signature verification
     * 
     * @return boolean
     */
    public boolean isSignatureValid() {
        return from.getSignature().verify(signedSignature, from.getSignature().getPublicKey());
    }

    /**
     * @return Operation
     */
    public Operation getOperation() {
        return operation;
    }

    /**
     * @return int
     */
    public int getPrice() {
        return price;
    }

    /**
     * @return Party
     */
    public Party getFrom() {
        return from;
    }

    /**
     * @return Party
     */
    public Party getTo() {
        return to;
    }

    /**
     * @return String
     */
    public String getSignedSignature() {
        return signedSignature;
    }
}
