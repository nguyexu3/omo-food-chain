package cz.cvut.fel.omo.blockchain;

import java.util.List;
import java.util.Date;
import java.nio.charset.StandardCharsets;
import com.google.common.hash.Hashing;

import cz.cvut.fel.omo.logging.Logger;

public class Block {

    private String hash;
    private String previousHash;
    private List<Transaction> transactions;
    private Date date;
    private int nonce = 0;

    public Block(String previousHash, List<Transaction> transactions, Date date) {
        this.previousHash = previousHash;
        this.transactions = transactions;
        this.date = date;
        this.hash = this.calculateHash();
    }

    /**
     * @return String
     */
    public String calculateHash() {
        String data = previousHash + date + nonce;
        for (Transaction t : transactions) {
            data += t.toString();
        }
        return Hashing.sha256().hashString(data, StandardCharsets.UTF_8).toString();
    }

    /**
     * mine the block using the proof-of-work concept
     * 
     * @param difficulity
     */
    public void mineBlock(int difficulity) {
        Logger.logBlockChain("Mining block");

        String expectedSubstring = new String(new char[difficulity]).replace('\0', '0');
        while (!hash.substring(0, difficulity).equals(expectedSubstring)) {
            this.nonce++;
            this.hash = this.calculateHash();
        }
    }

    /**
     * @return String
     */
    public String getHash() {
        return hash;
    }

    /**
     * @param hash
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * @return String
     */
    public String getPreviousHash() {
        return previousHash;
    }

    /**
     * @param previousHash
     */
    public void setPreviousHash(String previousHash) {
        this.previousHash = previousHash;
    }

    /**
     * @return List<Transaction>
     */
    public List<Transaction> getTransactions() {
        return transactions;
    }

    /**
     * @param transactions
     */
    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    /**
     * @return Date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return int
     */
    public int getNonce() {
        return nonce;
    }

    /**
     * @param nonce
     */
    public void setNonce(int nonce) {
        this.nonce = nonce;
    }
}
