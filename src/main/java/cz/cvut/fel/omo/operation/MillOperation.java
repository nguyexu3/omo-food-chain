package cz.cvut.fel.omo.operation;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.party.Party;

public class MillOperation extends Operation {

    private EMethodType method;
    private int days;

    public MillOperation(Food food, Party party, EMethodType method, int days) {
        super(EOperation.MILL, food, party);
        this.method = method;
        this.days = days;
    }

    /**
     * @return String
     */
    @Override
    public String getParameters() {
        return "method: " + method.getName() + ", days: " + Integer.toString(days);
    }

    /**
     * @return EMethodType
     */
    public EMethodType getMethod() {
        return method;
    }

    /**
     * @param method
     */
    public void setMethod(EMethodType method) {
        this.method = method;
    }

    /**
     * @return int
     */
    public int getDays() {
        return days;
    }

    /**
     * @param days
     */
    public void setDays(int days) {
        this.days = days;
    }
}
