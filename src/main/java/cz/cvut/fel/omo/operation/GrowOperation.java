package cz.cvut.fel.omo.operation;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.party.Party;

public class GrowOperation extends Operation {

    private int days;
    private String fertiliser;
    private String place;

    public GrowOperation(Food food, Party party, int days, String fertiliser, String place) {
        super(EOperation.GROW, food, party);
        this.days = days;
        this.fertiliser = fertiliser;
        this.place = place;
    }

    /**
     * @return String
     */
    @Override
    public String getParameters() {
        return "days: " + Integer.toString(days) + ", fertiliser: " + fertiliser + ", place: " + place;
    }

    /**
     * @return int
     */
    public int getDays() {
        return days;
    }

    /**
     * @param days
     */
    public void setDays(int days) {
        this.days = days;
    }

    /**
     * @return String
     */
    public String getFertiliser() {
        return fertiliser;
    }

    /**
     * @param fertiliser
     */
    public void setFertiliser(String fertiliser) {
        this.fertiliser = fertiliser;
    }

    /**
     * @return String
     */
    public String getPlace() {
        return place;
    }

    /**
     * @param place
     */
    public void setPlace(String place) {
        this.place = place;
    }
}
