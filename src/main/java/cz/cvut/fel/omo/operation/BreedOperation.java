package cz.cvut.fel.omo.operation;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.party.Party;

public class BreedOperation extends Operation {

    private int days;
    private String feed;
    private String place;

    public BreedOperation(Food food, Party party, int days, String feed, String place) {
        super(EOperation.BREED, food, party);
        this.days = days;
        this.feed = feed;
        this.place = place;
    }

    /**
     * @return String
     */
    @Override
    public String getParameters() {
        return "days: " + Integer.toString(days) + ", feed: " + feed + ", place: " + place;
    }

    /**
     * @return int
     */
    public int getDays() {
        return days;
    }

    /**
     * @param days
     */
    public void setDays(int days) {
        this.days = days;
    }

    /**
     * @return String
     */
    public String getFeed() {
        return feed;
    }

    /**
     * @param feed
     */
    public void setFeed(String feed) {
        this.feed = feed;
    }

    /**
     * @return String
     */
    public String getPlace() {
        return place;
    }

    /**
     * @param place
     */
    public void setPlace(String place) {
        this.place = place;
    }
}
