package cz.cvut.fel.omo.operation;

public enum EMethodType {

    HAND("hand"), MACHINE("machine");

    String name;

    EMethodType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
