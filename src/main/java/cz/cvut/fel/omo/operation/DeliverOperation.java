package cz.cvut.fel.omo.operation;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.party.Party;

public class DeliverOperation extends Operation {

    private double hours;
    private String destination;

    public DeliverOperation(Food food, Party party, double hours, String destination) {
        super(EOperation.DELIVER, food, party);
        this.hours = hours;
        this.destination = destination;
    }

    /**
     * @return String
     */
    @Override
    public String getParameters() {
        return "hours: " + Double.toString(hours) + ", destination: " + destination;
    }

    /**
     * @return double
     */
    public double getHours() {
        return hours;
    }

    /**
     * @param hours
     */
    public void setHours(double hours) {
        this.hours = hours;
    }

    /**
     * @return String
     */
    public String getDestination() {
        return destination;
    }

    /**
     * @param destination
     */
    public void setDestination(String destination) {
        this.destination = destination;
    }
}
