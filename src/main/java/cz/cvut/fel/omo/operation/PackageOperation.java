package cz.cvut.fel.omo.operation;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.party.Party;

public class PackageOperation extends Operation {

    private EMethodType method;
    private double weight;
    private String material;

    public PackageOperation(Food food, Party party, EMethodType method, double weight, String material) {
        super(EOperation.PACKAGE, food, party);
        this.method = method;
        this.weight = weight;
        this.material = material;
    }

    /**
     * @return String
     */
    @Override
    public String getParameters() {
        return "method: " + method.getName() + ", weight: " + Double.toString(weight) + ", material: " + material;
    }

    /**
     * @return EMethodType
     */
    public EMethodType getMethod() {
        return method;
    }

    /**
     * @param method
     */
    public void setMethod(EMethodType method) {
        this.method = method;
    }

    /**
     * @return double
     */
    public double getWeight() {
        return weight;
    }

    /**
     * @param weight
     */
    public void setWeight(double weight) {
        this.weight = weight;
    }

    /**
     * @return String
     */
    public String getMaterial() {
        return material;
    }

    /**
     * @param material
     */
    public void setMaterial(String material) {
        this.material = material;
    }
}
