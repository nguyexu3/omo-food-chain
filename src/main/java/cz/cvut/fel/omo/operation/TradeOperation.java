package cz.cvut.fel.omo.operation;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.party.Party;

public class TradeOperation extends Operation {

    public TradeOperation(Food food, Party party) {
        super(EOperation.TRADE, food, party);
    }

    /**
     * @return String
     */
    @Override
    public String getParameters() {
        return "";
    }
}
