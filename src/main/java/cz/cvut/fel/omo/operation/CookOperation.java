package cz.cvut.fel.omo.operation;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.party.Party;

public class CookOperation extends Operation {

    private EMethodType method;
    private String temperature;

    public CookOperation(Food food, Party party, EMethodType method, String temperature) {
        super(EOperation.COOK, food, party);
        this.method = method;
        this.temperature = temperature;
    }

    /**
     * @return String
     */
    @Override
    public String getParameters() {
        return "method: " + method.getName() + ", temperature: " + temperature;
    }

    /**
     * @return EMethodType
     */
    public EMethodType getMethod() {
        return method;
    }

    /**
     * @param method
     */
    public void setMethod(EMethodType method) {
        this.method = method;
    }

    /**
     * @return String
     */
    public String getTemperature() {
        return temperature;
    }

    /**
     * @param temperature
     */
    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }
}
