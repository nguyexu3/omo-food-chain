package cz.cvut.fel.omo.operation;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.party.Party;

public class StoreOperation extends Operation {

    private int days;
    private String temperature;
    private String humility;
    private String place;
    private int weight;

    public StoreOperation(Food food, Party party, int days, String temperature, String humility, String place,
            int weight) {
        super(EOperation.STORE, food, party);
        this.days = days;
        this.temperature = temperature;
        this.humility = humility;
        this.place = place;
        this.weight = weight;
    }

    /**
     * @return String
     */
    @Override
    public String getParameters() {
        return "days: " + Integer.toString(days) + ", temperature: " + temperature + ", humility: " + humility
                + ", place: " + place + ", weight: " + Integer.toString(weight) + "kg";
    }

    /**
     * @return int
     */
    public int getDays() {
        return days;
    }

    /**
     * @param days
     */
    public void setDays(int days) {
        this.days = days;
    }

    /**
     * @return String
     */
    public String getTemperature() {
        return temperature;
    }

    /**
     * @param temperature
     */
    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    /**
     * @return String
     */
    public String getHumility() {
        return humility;
    }

    /**
     * @param humility
     */
    public void setHumility(String humility) {
        this.humility = humility;
    }

    /**
     * @return String
     */
    public String getPlace() {
        return place;
    }

    /**
     * @param place
     */
    public void setPlace(String place) {
        this.place = place;
    }

    /**
     * @return int
     */
    public int getWeight() {
        return weight;
    }

    /**
     * @param weight
     */
    public void setWeight(int weight) {
        this.weight = weight;
    }
}
