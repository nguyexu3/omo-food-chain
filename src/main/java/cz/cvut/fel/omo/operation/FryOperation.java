package cz.cvut.fel.omo.operation;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.party.Party;

public class FryOperation extends Operation {

    private double hours;

    public FryOperation(Food food, Party party, double hours) {
        super(EOperation.FRY, food, party);
        this.hours = hours;
    }

    /**
     * @return String
     */
    @Override
    public String getParameters() {
        return "hours: " + Double.toString(hours);
    }

    /**
     * @return double
     */
    public double getHours() {
        return hours;
    }

    /**
     * @param hours
     */
    public void setHours(double hours) {
        this.hours = hours;
    }
}
