package cz.cvut.fel.omo.operation;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.party.Party;

public class CleanOperation extends Operation {

    private EMethodType method;

    public CleanOperation(Food food, Party party, EMethodType method) {
        super(EOperation.CLEAN, food, party);
        this.method = method;
    }

    /**
     * @return String
     */
    @Override
    public String getParameters() {
        return "method: " + method.getName();
    }

    /**
     * @return EMethodType
     */
    public EMethodType getMethod() {
        return method;
    }

    /**
     * @param method
     */
    public void setMethod(EMethodType method) {
        this.method = method;
    }
}
