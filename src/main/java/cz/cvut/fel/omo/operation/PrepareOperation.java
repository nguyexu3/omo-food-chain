package cz.cvut.fel.omo.operation;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.party.Party;

public class PrepareOperation extends Operation {

    private EMethodType method;

    public PrepareOperation(Food food, Party party, EMethodType method) {
        super(EOperation.PREPARE, food, party);
        this.method = method;
    }

    /**
     * @return String
     */
    @Override
    public String getParameters() {
        return "method: " + method.getName();
    }

    /**
     * @return EMethodType
     */
    public EMethodType getMethod() {
        return method;
    }

    /**
     * @param method
     */
    public void setMethod(EMethodType method) {
        this.method = method;
    }
}
