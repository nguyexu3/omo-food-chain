package cz.cvut.fel.omo.operation;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.party.Party;

/**
 * We store the operation enum so we could more easily filter out operations
 * when a channel unsubscribes from a particular operation
 */
abstract public class Operation {

    private EOperation type;
    private Food food;
    private Party party; // party is the entity which executes the operation

    public Operation(EOperation type, Food food, Party party) {
        this.type = type;
        this.food = food;
        this.party = party;
    }

    /**
     * @param getType(
     * @return String
     */
    public abstract String getParameters();

    /**
     * @return EOperation
     */
    public EOperation getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(EOperation type) {
        this.type = type;
    }

    /**
     * @return Food
     */
    public Food getFood() {
        return food;
    }

    /**
     * @param food
     */
    public void setFood(Food food) {
        this.food = food;
    }

    /**
     * @return Party
     */
    public Party getParty() {
        return party;
    }

    /**
     * @param party
     */
    public void setParty(Party party) {
        this.party = party;
    }
}
