package cz.cvut.fel.omo.operation;

import cz.cvut.fel.omo.food.Food;
import cz.cvut.fel.omo.party.Party;

public class ButcherOperation extends Operation {

    private String personId;

    public ButcherOperation(Food food, Party party, String personId) {
        super(EOperation.BUTCHER, food, party);
        this.personId = personId;
    }

    /**
     * @return String
     */
    @Override
    public String getParameters() {
        return "personId: " + personId;
    }

    /**
     * @return String
     */
    public String getPersonId() {
        return personId;
    }

    /**
     * @param personId
     */
    public void setPersonId(String personId) {
        this.personId = personId;
    }
}
