package cz.cvut.fel.omo.operation;

public enum EOperation {

    GROW("grow"), BREED("breed"), STORE("store"), COOK("cook"), CLEAN("clean"), FRY("fry"), PACKAGE("package"),
    MILL("mill"), BAKE("bake"), PREPARE("prepare"), BUTCHER("butcher"), TRADE("trade"), DELIVER("deliver");

    String name;

    EOperation(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
