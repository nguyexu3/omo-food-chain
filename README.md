# OMO Semester work: Food chain

The main goal of this project is simulate a blockchain system for the food system. It should be able to trace all operations for a particular food processed and consumed throught of a variaties of parties. Its implemented for single-thread usage.

- _Notice: class diagram can be found in the /doc folder_ \*

## Requirements

**F1**
Parties are implemented in the `omo/party` folder

**F2**
Each party can create a operation (found in `/omo/operation`) which has a Food as a subject and specific parameters of the operations.
Then we can create a transaction (found in `/omo/blockchain/Transaction`) between two parties, operation and the price of this transaction.

**F3**
The current blockchain system is fully not completed to be called a true blockchain, nevertheless it has some of it core ideas.

Theres a single blockchain shared between all entities, which stores blocks. During the initialization we create the first genesis block. Each next block has a hash created by hashing the previous hash and the data.

To simulate mining we mimicked it with the `mineBlock()` method which tries to create the hash as its proof of work. This is ensures that non of the previous blocks are tempered.

Practially in this simulation you could temper with the block object but the hash is our source of truth which tells holds the non edited data.

I simplified the data to a string format which holds the operations, price and transaction info. We could had used a JSON format to revert it back to an object.

If someone tempered with the properties of the object. We verify it but creating the new block sequence, check if with the `isChainValid()` method and compare it with our current block sequence.

**F4**
Non valid transactions arent added to the blockchain.
To generate the report we use the `/omo/logging/report` class.

**F5**
Each party subscribes to a channel, which is specified by its type (MeatChannel, VegetableChannel...). The channel holds all of the request for food processing/consumtion and emits it to its subscribers.

The part with "operation has defined in which channel is executed" from requirement we implemented it differently then intended. Instead, we defined in channels what request can i process.

**F6**
To detect the problem with double spending, we use the trick that real bitcoin uses too. The problem with double spending is that the entity process an operation on an unowned food after it did before.
To solve this, before we add the transaction to the blockchain we check if the entity even owns the food.
(theres an exception for farmers which creates new food)

**F7**
Not sure how this was meant to mean, but if someone tries to temper the blockchain by changing to content we use the `isChainValid()` method.

**F8**
Each food has stored its state, which can be updated with the `updtateState()` method.

**F9**
We simulated the approval of the parties through the `accept()` method, but the request isnt fully executed until `process()` is called. To process a request with particular food we use `process(food)` used for example during trading or processing of a certain food.

Then theres this feature that a subscriber can unsubscribe from a specific operation type or the whole channel.

**F10**
Reports are generated using the transactions fromt the blockchain in the `/omo/logging/report` class.

## Design parameters

State pattern

- changing the food (example from potatoe -> cleaned potatoe);
- Found in `/omo/food/`

Factory pattern

- ease of creating class instances
- Found in `/omo/channel/`

Observer pattern

- bidirectional observer pattern in channel to propagate request and cancel accepted requests
- Found in `/omo/channel/Channel`

Singleton

- to assure that one instance of blockchain is used
- Found in `/omo/blockchain/BlockchainProvider`
